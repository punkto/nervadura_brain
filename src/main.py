#!/usr/bin/env python3
import paho.mqtt.client as mqtt
import time
import sys
import json

try:
    import defaults
    from processor.processor import Processor as Processor
except ModuleNotFoundError:
    # This happens when you execute pytest fro the repository root.
    # We need to add to the python path the root directory and use src in the module path.
    sys.path.append("./")
    import src.defaults
    from src.processor.processor import Processor as Processor


def send_messages(messages: list, client: mqtt.Client) -> None:
    for message in messages:
        client.publish(topic=message["topic"], payload=message["message"])


def on_message(client, userdata, message):
    m = userdata["processor"].process_new_message(msg=str(message.payload.decode("utf-8")),
                                                  topic=str(message.topic),
                                                  timestamp=time.time())
    send_messages(m, client)


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("#")


def on_disconnect(client, userdata, rc):
    print("Disconnected", rc)
    client.reconnect()


def get_dict_from_file(file_name: str) -> dict:
    with open(file_name, 'r') as file:
        return json.load(file)


def is_configuration_given_by_file() -> bool:
    return len(sys.argv) == 2


def get_configuration_file() -> str:
    return sys.argv[1]


def time_to_check_file_has_passed(last_check_ts:float, current_ts: float) -> bool:
    wait_seconds = 1.0
    return last_check_ts + wait_seconds <= current_ts


if __name__ == '__main__':
    config = defaults.config
    if is_configuration_given_by_file():
        print(f"Get configuration from file {get_configuration_file()}")
        sensor_actuator_mapping = get_dict_from_file(get_configuration_file())
        last_timestamp_configuration_file_was_checked = time.time()
    else:
        print(f"Get configuration from default")
        sensor_actuator_mapping = defaults.nervadura
        last_timestamp_configuration_file_was_checked = 0
    print(f"Sensor actuator configuration:\n{sensor_actuator_mapping}")
    the_processor = Processor(config=config, sensor_actuator_mapping=sensor_actuator_mapping)
    userdata = {"processor": the_processor}
    mqtt_client = mqtt.Client(userdata=userdata)
    mqtt_client.connect(config["mqtt_broker_address"])
    mqtt_client.on_connect = on_connect
    mqtt_client.on_message = on_message
    while True:
        mqtt_client.loop(timeout=1.0)
        messages_to_send = the_processor.loop(time.time())
        send_messages(messages_to_send, mqtt_client)
        if is_configuration_given_by_file():
            if time_to_check_file_has_passed(last_timestamp_configuration_file_was_checked, time.time()):
                new_configuration = get_dict_from_file(get_configuration_file())
                if new_configuration != sensor_actuator_mapping:
                    sensor_actuator_mapping = new_configuration
                    messages_to_send = the_processor.switch_off_actuators()
                    send_messages(messages_to_send, mqtt_client)
                    the_processor.update_mapping(sensor_actuator_mapping=sensor_actuator_mapping)
                    print(f"Sensor actuator configuration:\n{sensor_actuator_mapping}")
