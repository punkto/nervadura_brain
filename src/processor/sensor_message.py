class SensorMessage:
    """
    This class is used to store a message from a sensor and to decode it using getters.
    """

    def __init__(self, msg: str):
        self.msg = msg

    def get_sensor_id(self) -> str:
        return self.msg.split(sep=" ")[0]

    def get_value(self) -> int:
        return int(self.msg.split(sep=" ")[1])
