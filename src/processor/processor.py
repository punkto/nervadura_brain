import pprint
import random

try:
    from processor.sensor_message import SensorMessage
except ModuleNotFoundError:
    # This happens when you execute pytest fro the repository root.
    # We need to add to the python path the root directory and use src in the module path.
    import sys

    sys.path.append("./")
    from src.processor.sensor_message import SensorMessage


def parse_config(config: dict) -> dict:
    """
    Processor class helper.

    :param config:
    :return:
    """
    # TODO check config
    return config.copy()


def parse_sensor_actuator_mapping(sensor_actuator_mapping: dict) -> dict:
    """
    Processor class helper.

    :param sensor_actuator_mapping:
    :return:
    """
    # TODO check config

    res = {}
    for sensor in sensor_actuator_mapping:
        res[sensor] = sensor_actuator_mapping[sensor].copy()
        res[sensor]["last value sent"] = 0  # The last value sent to an actuator
        res[sensor]['last message ts'] = 0  # The last ts where a message was sent
    return res


def _must_send_zero(sensor_data: dict, timestamp: float) -> bool:
    """
    Helper for Processor.loop method.

    Returns if the loop must send a 0 to the actuator given the current state of the sensor and the current timestamp.

    :param sensor_data:
    :param timestamp:
    :return:
    """
    return sensor_data['last value sent'] == 1 and \
           timestamp > sensor_data['last message ts'] + sensor_data['send 0 after seconds']


def _update_current_actuator_index(sensor_data: dict) -> int:
    """
    Helper for _process_sensor_value_update
    throws LookupError if "sequence type" is not valid.

    :param sensor_data:
    :return:
    """
    if sensor_data["sequence type"] == "sequential":
        return (sensor_data['current actuator index'] + 1) % len(sensor_data['actuators'])
    if sensor_data["sequence type"] == "random":
        return random.randrange(0, len(sensor_data['actuators']))

    raise LookupError


class Processor:
    """
    This class maintains the dict that describes the current status of sensors and which
    actuators are triggering. This dict is based on the src.defaults.nervadura structure.
    """

    def __init__(self, config: dict, sensor_actuator_mapping: dict) -> None:
        """

        :param config: Main configuration based on the src.defaults.config structure.
        :param sensor_actuator_mapping: Sensors-actuators configuration based on the src.defaults.nervadura structure.
        """
        self.config = parse_config(config)
        self.sensor_actuator_mapping = parse_sensor_actuator_mapping(sensor_actuator_mapping)

    def update_mapping(self, sensor_actuator_mapping: dict) -> None:
        self.sensor_actuator_mapping = parse_sensor_actuator_mapping(sensor_actuator_mapping)

    def switch_off_actuators(self) -> list:
        """
        Generates messages to switch off all actuators and updates self.sensor_actuator_mapping accordingly.

        :return:
        """
        messages_to_send = []
        for sensor_id in self.sensor_actuator_mapping:
            sensor_data = self.sensor_actuator_mapping[sensor_id]  # This is just to shorten the code
            if sensor_data['last value sent'] == 0:
                continue
            sensor_data['last value sent'] = 0
            messages_to_send.append(
                {"topic": self.config["actuators topic"],
                 "message": "{} 0".format(sensor_data['actuators'][sensor_data['current actuator index']])})
        return messages_to_send

    def process_new_message(self, msg: str, topic: str, timestamp: float) -> list:
        """
        Does the processing of an incoming message.

        Execute this method when a new message arrives.

        :param timestamp:
        :param msg:
        :param topic:
        :return: list of dicts with the messages that have to be sent, Example:
            [{"topic": "nervadura/actuator_triggered", "message": "act1 1"}]
        """
        messages_to_send = []
        print("[{2}] topic-> {0} msg-> {1}".format(topic, msg, timestamp))
        if topic == self.config["sensors topic"]:
            command = SensorMessage(msg=msg)
            print("message from sensor {} with value {}".format(command.get_sensor_id(), command.get_value()))
            messages = self._process_sensor_value_update(
                sensor_data=self.sensor_actuator_mapping[command.get_sensor_id()],
                value=command.get_value(),
                timestamp=timestamp)
            messages_to_send.extend(messages)
            # print("config")
            # pprint.pprint(self.config)
            # print("sensor_actuator_mapping")
            # pprint.pprint(self.sensor_actuator_mapping)
        return messages_to_send

    def _process_sensor_value_update(self, sensor_data: dict, value: int, timestamp: float) -> list:
        """

        :param sensor_data: the sensor_actuator_mapping part for a specific sensor.
        :param value:
        :param timestamp:
        :return: list of dicts with the messages that have to be sent, Example:
            [{"topic": "nervadura/actuator_triggered", "message": "act1 1"}]
        """
        messages_to_send = []
        if value == 1:
            if timestamp > sensor_data['last message ts'] + sensor_data['cool down seconds']:
                try:
                    sensor_data['current actuator index'] = _update_current_actuator_index(sensor_data=sensor_data)
                except LookupError:
                    print(f'This sensor is not well configured: wrong "sequence type" \n{sensor_data}')

                sensor_data['last value sent'] = 1
                sensor_data['last message ts'] = timestamp
                messages_to_send.append(
                    {"topic": self.config["actuators topic"],
                     "message": "{} 1".format(sensor_data['actuators'][sensor_data['current actuator index']])})

        return messages_to_send

    def loop(self, timestamp: float) -> list:
        """
        Performs maintenance tasks.
        Execute this method often in your main loop.

        :param timestamp:
        :return: list of dicts with the messages that have to be sent, Example:
            [{"topic": "nervadura/actuator_triggered", "message": "act1 1"}]
        """
        messages_to_send = []
        for sensor_id in self.sensor_actuator_mapping:
            sensor_data = self.sensor_actuator_mapping[sensor_id]  # This is just to shorten the code
            if _must_send_zero(sensor_data=sensor_data, timestamp=timestamp):
                sensor_data['last value sent'] = 0
                messages_to_send.append(
                    {"topic": self.config["actuators topic"],
                     "message": "{} 0".format(sensor_data['actuators'][sensor_data['current actuator index']])})

        return messages_to_send
