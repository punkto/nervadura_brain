#!/usr/bin/env python3

config = {
    "mqtt_broker_address": "192.168.8.152",
    "devices topic": "nervadura/devices",
    "sensors topic": "nervadura/sensor_triggered",
    "actuators topic": "nervadura/actuator_triggered"
}


'''
nervadura sensor-actuator connection configuration:
{
    "SENSOR_1_ID": 
    {
        "sequence type": "sequential",
        "actuators": ["$ACTUATOR_ID_1", "$ACTUATOR_ID_2"],
        "current actuator index": 0,
        "cool down seconds": 10,
        "send 0 after seconds": 5
    },
    "SENSOR_2_ID": 
    {
        "sequence type": "random",
        "actuators": ["$ACTUATOR_ID_1", "$ACTUATOR_ID_2"],
        "current actuator index": 0,
        "cool down seconds": 10.0,
        "send 0 after seconds": 5.0
    },
    ...
}

Tags:
 - "sequence type": Mandatory. String. How the application decides the next actuator to address. Currently can be:
   - "sequential"
   - "random"
 - "actuators": Mandatory. Array of strings with the IDs of the actuators that this sensor addresses.
 - "current actuator index": Optional. Integer. The last actuator that has been fired.
 - "cool down seconds": Optional. Float. Seconds since the last actuator trigger when the actuator can be triggered 
                        again.
 - "send 0 after seconds": Optional. Float. Seconds since the last actuator trigger when the app sends a 0.
'''
nervadura = {
    "sensor-28de0": {
        "sequence type": "random",
        "actuators": ["ACT_ID_1", "ACT_ID_2", "ACT_ID_3", "ACT_ID_4"],
        "current actuator index": -1,
        "cool down seconds": 5,
        "send 0 after seconds": 2.5
    }
}
