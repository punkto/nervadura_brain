# Nervadura: brain

This is the code of the application that connects the sensors with the actuators, given a configuration.

# Install
You need Python 3.7+ and pip. Then install the needed Python packages
```
$ pip3 install -r ./packages/packages.txt
```

# Configuration files
Read ./src/defaults.py for the default configurations and format.

You can launch the app with the name of a file with the sensor-actuator configuration in JSON format. In this case, the app checks if that file changes and applies the new configuration if it is the case.

# Run
```
$ cd src
src $ python3 main.py [configuration file path]
```

# Testing
```
$ pytest --cov=src tests/unit/ -vv
```

# TODO
 - complete this TODO :p
 