import sys

# This is needed to execute pytest from the repository root. We need to add to the python path the root directory and
# use src in the module path so Python can find the src modules.
sys.path.append("./")
from src.processor.processor import Processor as Processor


def test_processor_parse_new_message():
    tests = [
        {"number": 1, "msg": "sensor-28de0 1", "topic": "", "timestamp": 1000,
         "expected_messages": [],
         "expected_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'sequential', 'actuators': ['ACT_ID_1', 'ACT_ID_2'],
                              'current actuator index': -1, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 0, 'last message ts': 0}}
         },
        {"number": 2, "msg": "sensor-28de0 1", "topic": "", "timestamp": 1000.1,
         "expected_messages": [],
         "expected_sensor_actuator_mapping": {
             "sensor-28de0": {"sequence type": "sequential", "actuators": ["ACT_ID_1", "ACT_ID_2"],
                              "current actuator index": -1, "cool down seconds": 10, "send 0 after seconds": 5,
                              'last value sent': 0, 'last message ts': 0}}
         },
        {"number": 3, "msg": "sensor-28de0 1", "topic": "nervadura/sensor_triggered", "timestamp": 1000.2,
         "expected_messages": [{'message': 'ACT_ID_1 1', 'topic': 'nervadura/actuator_triggered'}],
         "expected_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'sequential', 'actuators': ['ACT_ID_1', 'ACT_ID_2'],
                              'current actuator index': 0, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 1, 'last message ts': 1000.2}}
         },
        {"number": 4, "msg": "sensor-28de0 1", "topic": "nervadura/sensor_triggered", "timestamp": 1001.2,
         "expected_messages": [],
         "expected_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'sequential', 'actuators': ['ACT_ID_1', 'ACT_ID_2'],
                              'current actuator index': 0, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 1, 'last message ts': 1000.2}}
         },
        {"number": 5, "msg": "sensor-28de0 1", "topic": "nervadura/sensor_triggered", "timestamp": 1002.2,
         "expected_messages": [],
         "expected_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'sequential', 'actuators': ['ACT_ID_1', 'ACT_ID_2'],
                              'current actuator index': 0, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 1, 'last message ts': 1000.2}}
         },
        {"number": 6, "msg": "sensor-28de0 0", "topic": "nervadura/sensor_triggered", "timestamp": 1003.2,
         "expected_messages": [],
         "expected_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'sequential', 'actuators': ['ACT_ID_1', 'ACT_ID_2'],
                              'current actuator index': 0, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 1, 'last message ts': 1000.2}}
         },
        {"number": 7, "msg": "sensor-28de0 0", "topic": "nervadura/sensor_triggered", "timestamp": 1013.2,
         "expected_messages": [],
         "expected_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'sequential', 'actuators': ['ACT_ID_1', 'ACT_ID_2'],
                              'current actuator index': 0, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 1, 'last message ts': 1000.2}}
         },
        {"number": 8, "msg": "sensor-28de0 0", "topic": "nervadura/sensor_triggered", "timestamp": 1023.2,
         "expected_messages": [],
         "expected_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'sequential', 'actuators': ['ACT_ID_1', 'ACT_ID_2'],
                              'current actuator index': 0, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 1, 'last message ts': 1000.2}}
         }
    ]

    config = {"mqtt_broker_address": "192.168.8.152", "devices topic": "nervadura/devices",
              "sensors topic": "nervadura/sensor_triggered", "actuators topic": "nervadura/actuator_triggered"}

    sensor_actuator_mapping = {
        "sensor-28de0": {
            "sequence type": "sequential",
            "actuators": ["ACT_ID_1", "ACT_ID_2"],
            "current actuator index": -1,
            "cool down seconds": 10,
            "send 0 after seconds": 5
        }
    }

    processor = Processor(config=config, sensor_actuator_mapping=sensor_actuator_mapping)

    def do_test(test: dict) -> None:
        output_messages = processor.process_new_message(msg=test["msg"],
                                                        topic=test["topic"],
                                                        timestamp=test["timestamp"])
        assert [test["number"], test["expected_messages"]] == [test["number"], output_messages]
        assert [test["number"], test["expected_sensor_actuator_mapping"]] == \
               [test["number"], processor.sensor_actuator_mapping]

    [do_test(test) for test in tests]


def test_processor_loop():
    tests = [
        {"number": 1, "timestamp": 1000,
         "current_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'sequential', 'actuators': ['ACT_ID_1', 'ACT_ID_2'],
                              'current actuator index': -1, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 0, 'last message ts': 0}},
         "expected_messages": [],
         "expected_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'sequential', 'actuators': ['ACT_ID_1', 'ACT_ID_2'],
                              'current actuator index': -1, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 0, 'last message ts': 0}}
         },
        {"number": 2, "timestamp": 1000,
         "current_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'sequential', 'actuators': ['ACT_ID_1', 'ACT_ID_2'],
                              'current actuator index': -1, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 1, 'last message ts': 0}},
         "expected_messages": [{'message': 'ACT_ID_2 0', 'topic': 'nervadura/actuator_triggered'}],
         "expected_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'sequential', 'actuators': ['ACT_ID_1', 'ACT_ID_2'],
                              'current actuator index': -1, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 0, 'last message ts': 0}}
         },
    ]

    config = {"mqtt_broker_address": "192.168.8.152", "devices topic": "nervadura/devices",
              "sensors topic": "nervadura/sensor_triggered", "actuators topic": "nervadura/actuator_triggered"}

    sensor_actuator_mapping = {
        "sensor-28de0": {
            "sequence type": "sequential",
            "actuators": ["ACT_ID_1", "ACT_ID_2"],
            "current actuator index": -1,
            "cool down seconds": 10,
            "send 0 after seconds": 5
        }
    }

    processor = Processor(config=config, sensor_actuator_mapping=sensor_actuator_mapping)

    def do_test(test: dict) -> None:
        processor.sensor_actuator_mapping = test["current_sensor_actuator_mapping"]
        output_messages = processor.loop(timestamp=test["timestamp"])
        assert [test["number"], test["expected_messages"]] == [test["number"], output_messages]
        assert [test["number"], test["expected_sensor_actuator_mapping"]] == \
               [test["number"], processor.sensor_actuator_mapping]

    [do_test(test) for test in tests]


def test_processor_should_not_send_two_consecutive_zeros_if_a_1_is_sent_after_5_secs():
    config = {"mqtt_broker_address": "192.168.8.152", "devices topic": "nervadura/devices",
              "sensors topic": "nervadura/sensor_triggered", "actuators topic": "nervadura/actuator_triggered"}

    sensor_actuator_mapping = {
        "sensor-28de0": {
            "sequence type": "sequential",
            "actuators": ["ACT_ID_1", "ACT_ID_2"],
            "current actuator index": -1,
            "cool down seconds": 10,
            "send 0 after seconds": 5
        }
    }

    processor = Processor(config=config, sensor_actuator_mapping=sensor_actuator_mapping)
    # processor receives a 1 from a sensor
    processor.process_new_message(msg="sensor-28de0 1", topic="nervadura/sensor_triggered", timestamp=1000.0)
    # after 5 seconds, the processor sends a 0 to the actuator
    for ts in [1001.0, 1002.0, 1003.0, 1004.0, 1005.0]:
        msgs = processor.loop(timestamp=ts)
        assert len(msgs) == 0
    msgs = processor.loop(timestamp=1005.1)
    assert msgs == [{'message': 'ACT_ID_1 0', 'topic': 'nervadura/actuator_triggered'}]
    # now we receive a new 1 from the sensor
    msgs = processor.process_new_message(msg="sensor-28de0 1", topic="nervadura/sensor_triggered", timestamp=1006.0)
    assert msgs == []
    # with the next loop it should send nothing
    msgs = processor.loop(timestamp=1007.0)
    assert len(msgs) == 0


def test_processor_parse_new_message_random_sequence_type():
    tests = [
        {"number": 1, "msg": "sensor-28de0 1", "topic": "", "timestamp": 1000,
         "expected_messages": [],
         "expected_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'random', 'actuators': ["ACT_ID_1", "ACT_ID_2", "ACT_ID_3", "ACT_ID_4"],
                              'current actuator index': -1, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 0, 'last message ts': 0}}
         },
        {"number": 2, "msg": "sensor-28de0 1", "topic": "", "timestamp": 1000.1,
         "expected_messages": [],
         "expected_sensor_actuator_mapping": {
             "sensor-28de0": {"sequence type": "random", "actuators": ["ACT_ID_1", "ACT_ID_2", "ACT_ID_3", "ACT_ID_4"],
                              "current actuator index": -1, "cool down seconds": 10, "send 0 after seconds": 5,
                              'last value sent': 0, 'last message ts': 0}}
         },
        {"number": 3, "msg": "sensor-28de0 1", "topic": "nervadura/sensor_triggered", "timestamp": 1000.2,
         "expected_messages": [{'message': 'ACT_ID_1 1', 'topic': 'nervadura/actuator_triggered'},
                               {'message': 'ACT_ID_2 1', 'topic': 'nervadura/actuator_triggered'},
                               {'message': 'ACT_ID_3 1', 'topic': 'nervadura/actuator_triggered'},
                               {'message': 'ACT_ID_4 1', 'topic': 'nervadura/actuator_triggered'}],
         "expected_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'random', 'actuators': ["ACT_ID_1", "ACT_ID_2", "ACT_ID_3", "ACT_ID_4"],
                              'current actuator index': 0, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 1, 'last message ts': 1000.2}}
         },
        {"number": 4, "msg": "sensor-28de0 1", "topic": "nervadura/sensor_triggered", "timestamp": 1001.2,
         "expected_messages": [],
         "expected_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'random', 'actuators': ["ACT_ID_1", "ACT_ID_2", "ACT_ID_3", "ACT_ID_4"],
                              'current actuator index': 0, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 1, 'last message ts': 1000.2}}
         },
        {"number": 5, "msg": "sensor-28de0 1", "topic": "nervadura/sensor_triggered", "timestamp": 1002.2,
         "expected_messages": [],
         "expected_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'random', 'actuators': ["ACT_ID_1", "ACT_ID_2", "ACT_ID_3", "ACT_ID_4"],
                              'current actuator index': 0, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 1, 'last message ts': 1000.2}}
         },
        {"number": 6, "msg": "sensor-28de0 0", "topic": "nervadura/sensor_triggered", "timestamp": 1003.2,
         "expected_messages": [],
         "expected_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'random', 'actuators': ["ACT_ID_1", "ACT_ID_2", "ACT_ID_3", "ACT_ID_4"],
                              'current actuator index': 0, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 1, 'last message ts': 1000.2}}
         },
        {"number": 7, "msg": "sensor-28de0 0", "topic": "nervadura/sensor_triggered", "timestamp": 1013.2,
         "expected_messages": [],
         "expected_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'random', 'actuators': ["ACT_ID_1", "ACT_ID_2", "ACT_ID_3", "ACT_ID_4"],
                              'current actuator index': 0, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 1, 'last message ts': 1000.2}}
         },
        {"number": 8, "msg": "sensor-28de0 1", "topic": "nervadura/sensor_triggered", "timestamp": 1023.2,
         "expected_messages": [{'message': 'ACT_ID_1 1', 'topic': 'nervadura/actuator_triggered'},
                               {'message': 'ACT_ID_2 1', 'topic': 'nervadura/actuator_triggered'},
                               {'message': 'ACT_ID_3 1', 'topic': 'nervadura/actuator_triggered'},
                               {'message': 'ACT_ID_4 1', 'topic': 'nervadura/actuator_triggered'}],
         "expected_sensor_actuator_mapping": {
             'sensor-28de0': {'sequence type': 'random', 'actuators': ["ACT_ID_1", "ACT_ID_2", "ACT_ID_3", "ACT_ID_4"],
                              'current actuator index': 0, 'cool down seconds': 10, 'send 0 after seconds': 5,
                              'last value sent': 1, 'last message ts': 1000.2}}
         }
    ]

    config = {"mqtt_broker_address": "192.168.8.152", "devices topic": "nervadura/devices",
              "sensors topic": "nervadura/sensor_triggered", "actuators topic": "nervadura/actuator_triggered"}

    sensor_actuator_mapping = {
        "sensor-28de0": {
            "sequence type": "random",
            "actuators": ["ACT_ID_1", "ACT_ID_2", "ACT_ID_3", "ACT_ID_4"],
            "current actuator index": -1,
            "cool down seconds": 10,
            "send 0 after seconds": 5
        }
    }

    processor = Processor(config=config, sensor_actuator_mapping=sensor_actuator_mapping)

    def do_test(test: dict) -> None:
        output_messages = processor.process_new_message(msg=test["msg"],
                                                        topic=test["topic"],
                                                        timestamp=test["timestamp"])
        if output_messages or test["expected_messages"]:
            assert output_messages[0] in test["expected_messages"]

    [do_test(test) for test in tests]


def test_processor_switch_off_actuators():
    tests = [
        {
            "number": 1,
            "sensor_actuator_mapping": {
                'sensor-28de0': {'sequence type': 'random',
                                 'actuators': ["ACT_ID_1", "ACT_ID_2", "ACT_ID_3", "ACT_ID_4"],
                                 'current actuator index': 0, 'cool down seconds': 10, 'send 0 after seconds': 5,
                                 'last value sent': 1, 'last message ts': 1000.2}
            },
            "expected_messages": [{'message': 'ACT_ID_1 0', 'topic': 'nervadura/actuator_triggered'}],
            "expected_sensor_actuator_mapping": {
                'sensor-28de0': {'sequence type': 'random',
                                 'actuators': ['ACT_ID_1', 'ACT_ID_2', 'ACT_ID_3', 'ACT_ID_4'],
                                 'current actuator index': 0, 'cool down seconds': 10,
                                 'send 0 after seconds': 5, 'last value sent': 0, 'last message ts': 1000.2}}
        },
        {
            "number": 2,
            "sensor_actuator_mapping": {
                'sensor-28de0': {'sequence type': 'random',
                                 'actuators': ["ACT_ID_1", "ACT_ID_2", "ACT_ID_3", "ACT_ID_4"],
                                 'current actuator index': 0, 'cool down seconds': 10, 'send 0 after seconds': 5,
                                 'last value sent': 0, 'last message ts': 1000.2}
            },
            "expected_messages": [],
            "expected_sensor_actuator_mapping": {
                'sensor-28de0': {'sequence type': 'random',
                                 'actuators': ['ACT_ID_1', 'ACT_ID_2', 'ACT_ID_3', 'ACT_ID_4'],
                                 'current actuator index': 0, 'cool down seconds': 10,
                                 'send 0 after seconds': 5, 'last value sent': 0, 'last message ts': 1000.2}}
        }
    ]

    config = {"mqtt_broker_address": "192.168.8.152", "devices topic": "nervadura/devices",
              "sensors topic": "nervadura/sensor_triggered", "actuators topic": "nervadura/actuator_triggered"}

    def do_test(test: dict) -> None:
        processor = Processor(config=config, sensor_actuator_mapping={})
        processor.sensor_actuator_mapping = test["sensor_actuator_mapping"]
        output_messages = processor.switch_off_actuators()

        assert [test["number"], output_messages] == [test["number"], test["expected_messages"]]
        assert [test["number"], processor.sensor_actuator_mapping] == [
            test["number"], test["expected_sensor_actuator_mapping"]]

    [do_test(test) for test in tests]
