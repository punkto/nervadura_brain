import sys
# This is needed to execute pytest from the repository root. We need to add to the python path the root directory and
# use src in the module path so Python can find the src modules.
sys.path.append("./")
from src.processor.sensor_message import SensorMessage as SensorMessage


def test_sensor_message_getters():
    tests = [
        {"number": 1, "msg": "sensor-28de0 1", "sensor_id": "sensor-28de0", "value": 1},
        {"number": 2, "msg": "sensor-28de0 0", "sensor_id": "sensor-28de0", "value": 0},
        {"number": 3, "msg": "28de0 1", "sensor_id": "28de0", "value": 1}
    ]

    def do_test(test: dict) -> None:
        expected_output = [test["number"], test["sensor_id"], test["value"]]
        s = SensorMessage(msg=test["msg"])
        obtained_output = [test["number"], s.get_sensor_id(), s.get_value()]
        assert expected_output == obtained_output

    [do_test(test) for test in tests]
